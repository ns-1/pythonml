import numpy as np
import pandas


class Layer: 
    '''
    represents a layer in a neural network
    '''

# constructor
        
    def __init__(self, dim): 
        self.dim = dim
        
        
# Functions
    def init_values(self, l_after):
        '''
        Initializes the layer's weights and biases with samples from a normal distribution
        '''
        
        np.random.seed(1)
        self.weights = np.random.randn(self.dim, l_after)
        self.bias = np.random.randn(l_after, )
        
# Getters and Setters
    
    def get_dim(self):
        return self.dim
    
    def get_weights(self):
        return self.weights
    
    def get_bias(self):
        return self.bias
    
    def get_weighted_sum(self):
        return self.z
    
    def get_activation_value(self):
        return self.a
       
    def set_weights(self, w):
        self.weights = w
        
    
    def set_bias(self, b):
        self.bias = b
        
    def set_weighted_sum(self, Z):
        self.z = Z
        
        
    def set_activation_value(self, A):
        self.a = A


    
