# pythonML

## Overview

PythonML is a barebones implemenetation of a Feed Forward Neural Network in Python3. It is built with the capacity for three layers (input, hidden, output), and is designed with the intent of usage for classification problems. For simplicity and comprehensibility, it was designed in an object oriented manner

___

## Implementation

The user needs only to interface with the ```Network```  class to optimize an MLP model on their data. Since python has a large variety of third party packages available, I decided to leave all of the necessary matrix operations to third party packages, specifically ```numpy```. . Additionally, I left the dataset parsing operations up to user discretion. As long as the dataset can be parsed into numpy arrays, it is able to be used by PythonML. For my personal usage, I utilized ```pandas```  to parse datasets into numpy arrays.

### Layer.py

The ```Layer``` class is an object oriented representation of a layer in the neural network. This class is not directly interfaced by the user when optimizing an MLP model on their data, but serves as an object oriented representation of a layer to aid in ease of comprehensibility. Each layer object contains: weights, biases, weighted sum, and activation value, and there are various getter and setter functions which allow these to be read and modified. The constructor of the ```Layer```  class has the argument of ```dim```, which is the number of neurons in that layer. The layer is only fully instantiated once the ```init_values(l_after)``` function is called. The function ```init_values(l_after)``` has the argument ```l_after``` which is the number of neurons in the next layer. This function initializes weights and biases of the layer with samples from a normal distribution. For example, if a layer is instantiated as ```Layer(3)```  and initialized with values, and given the next layer has two neurons:


![layer](https://gitlab.com/ns-1/pythonml/-/raw/main/pictures/layer.png?ref_type=heads)

### Network.py

The ```network``` class allows for fitting an FFNN model on a dataset. First, the network topology is designed when instantiating a new ```Network```. The constructor of the ```Network``` class has the argument of a list of integers, which represent the amount of neurons in each layer. For example, the instantiation of ```new Network([4, 3, 3])``` would result in a network with the topology of 4 input layers, 3 hidden layers, and 3 output layers:

![layer](https://gitlab.com/ns-1/pythonml/-/raw/main/pictures/network433.png?ref_type=heads)

The instantiation of a ```Network``` creates a list of ```Layer``` objects with weights and biases initialized with samples from a normal distribution.

### Fit

To train a model on a given set of data, the function ```fit(x, y, iterations)``` is used, where ```x``` refers to the training inputs, ```y``` refers to the training outputs, and ```iterations``` refers to the number of training iterations. This function "trains" the neural network to be able to make predictions on data.

There are two substeps for fitting a neural network, namely forward propagation and backpropagation. 

### Forward Propagation

In each iteration of training the neural network, the ```forward_propagation()``` function computes the output of the neural network given a certain input.

The output is calculated via forward propagating the input through each layer in the neural network. This entails multiplying the input by the weights in the layer (mapping the input from its input dimension to the hidden layer dimension) and then adding the bias vector. Then, this weighted sum is passed through an activation function, which introduces non-linearity to the data (important for problems which aren't easily solved by linear based models). For my framework, I utilized ReLU and Sigmoid activation functions. 


### Backpropagation

After forward propagation is performed, the weights and biases for each layer must be updated accordingly given the error. To do this, neural networks utilize a concept known as backpropagation. The data is forward propagated through the network, yielding an output. This output is then utilized, a long with the correct outputs, to calculate the error in the network. 

Backpropagation calculates the gradient of the error function with respect to each layer's weights and biases. Using this calculated gradient, along with a step size, the weights and biases for each layer are updated.

### Predict

The forward and backpropagation steps are repeated for a certain number of iterations, until the neural network can effectively model the pattern present in the training data. 

From there, the neural network can be used to make predictions on observations. For this, the ```compute_output(x)``` function is used. The function has argument ```x```, which is the data to calculate a prediction for, and returns a predicted output. Like the ```forward_propagation()``` function, this function forward propagates an input through each layer in the network, resulting in a predicted output ```y```
___

## Conclusions

To conclude, via this project, I was able to implement a simple framework to optimize simple FFNN models through the use of computerized neural network equations.

As far as future improvements and projects go, I have a list of ideas to make models perform better and scale:

1. Utilize libraries which allow for GPU acceleration (Jax, PyTorch, Numba)
2. Design my library in a way that is resilient to overfitting (my method of choice for this is via the utilization of early stopping through a validation step which benchmarks model performance on a validation , could also add dropout)
3. Expand my library to allow for more hidden layers
4. Allow for the library to be agnostic to dataset representations (e.g. create an interface for the dataset)
5. Allow for more metric availability (error, precision, recall, F1-score)
6. Implement neural regression

Overall, this project served as my first venture into machine learning, and I will continue to build upon this foundation for future ventures. 