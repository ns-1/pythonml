import numpy as np
import matplotlib.pyplot as plt
import Layer as ly

class Network():
    '''
    A simple neural network with 1 hidden layer
    '''
    
# Variables

    learning_rate = .001
    
# Constructor
        
    def __init__(self, dims):
        '''
        Initializes a new Network with 1 hidden layer, each one containing the amount 
        of neurons in dims
        '''
        
        # Check if dims is correct length
        
        if len(dims) != 3:
            raise ValueError("len(dims) is not equal to len(3)")
        
        # Instantiate the neural network with a list of layers
        
        layers = []
        
        for x in range(0, len(dims)):
            layers.append(ly.Layer(dims[x]))
            
        self.layers = layers
        
        
        # Initialize each layer's bias and weights 
        
        for x in range(0, len(self.layers)-1):
            self.layers[x].init_values(self.layers[x+1].get_dim())
       
    
# Activation Functions
    
    def relu(self,x):
        '''
        Activation function which allows values greater than or equal to zero to pass through, but sets values less than zero to zero
        '''
        return np.maximum(0,x)
        
        
    def sigmoid(self,x):
        '''
        Activation function which takes any range of numbers and returns output between -1 and 1
        '''
        return 1/(1+np.exp(-x))

    
# Propogation
    
    def forward_propagation(self):
        '''
        Performs the forward propagation
        '''
        
        Z = self.X.dot(self.layers[0].get_weights()) + self.layers[0].get_bias()
        A = self.relu(Z)
        
        self.layers[0].set_weighted_sum(Z)
        self.layers[0].set_activation_value(A)
        
        Z1 = A.dot(self.layers[1].get_weights()) + self.layers[1].get_bias()
        
        self.layers[1].set_weighted_sum(Z)
            
        yhat = self.sigmoid(Z1)
   

        return yhat

    
    def back_propagation(self,yhat):
        '''
        Computes the gradient of the error and update weights and bias according.
        '''
        
        # Derivative of relu activation function
        def deriv_relu(x):
            x[x<=0] = 0
            x[x>0] = 1
            return x
        
        # Calculate the gradient with respect to yhat
        deriv_yhat = -(np.divide(self.y,yhat) - np.divide((1 - self.y),(1-yhat)))
        
        # Work backwards
        deriv_sig = yhat * (1-yhat)
        deriv_zf = deriv_yhat * deriv_sig
        
        
        # Calculate gradient for layer 2
        deriv_A1 = deriv_zf.dot(self.layers[1].get_weights().T)
        deriv_w2 = self.layers[0].get_activation_value().T.dot(deriv_zf)
        deriv_b2 = np.sum(deriv_zf, axis=0)
        
        # Calculae gradient for layer 1
        deriv_z1 = deriv_A1 * deriv_relu(self.layers[0].get_weighted_sum())
        deriv_w1 = self.X.T.dot(deriv_z1)
        deriv_b1 = np.sum(deriv_z1, axis=0)
        

        #update the weights and biases for layers 1 and 2
        self.layers[0].set_weights(self.layers[0].get_weights()- self.learning_rate * deriv_w1)
        self.layers[0].set_bias(self.layers[0].get_bias()- self.learning_rate * deriv_b1)
        
        
        self.layers[1].set_weights(self.layers[1].get_weights()- self.learning_rate * deriv_w2)
        self.layers[1].set_bias(self.layers[1].get_bias() - self.learning_rate * deriv_b2)                        
        
        
# Methods for training and testing

        
    def fit(self, X, y, iterations):
        '''
        Trains the neural network using the specified data and labels
        '''
        self.X = X
        self.y = y

        # Feed forward and backpropagate for the specified amount of iterations
        for i in range(iterations):
            yhat = self.forward_propagation()
            
            self.back_propagation(yhat)
            
            
    def compute_output(self, X):
        '''
        Predicts output using test data
        '''
        
        # Calculate weighted sum for first layer
        
        Z = X.dot(self.layers[0].get_weights()) + self.layers[0].get_bias() 
        
        # Pass through activation function
        
        A = self.relu(Z)
        
        # Calculate weighted sum for second layer
        
        Z1 = A.dot(self.layers[1].get_weights()) + self.layers[1].get_bias()
        
        # Pass through sigmoid function and return 
        
        return np.round(self.sigmoid(Z1))   